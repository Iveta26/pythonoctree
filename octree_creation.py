import pandas as pd
import matplotlib.pyplot as plt
import lasio
import laspy
import numpy as np
import trimesh
import open3d as o3d

filename = "Data/2743_1234.las"
newfile = "dataCloud.ply"

las = laspy.read(filename)
N = 2000


# creating point cloud in ply format
def create_point_cloud(las):
    point_data = np.stack([np.float64(las.X), np.float64(las.Y), np.float64(las.Z)], axis=0).transpose(
        (1, 0))  # point cloud data
    print(point_data)
    # the method Vector3dVector() will convert numpy array of shape (n, 3) to Open3D format.
    point_cloud = o3d.geometry.PointCloud()
    point_cloud.points = o3d.utility.Vector3dVector(point_data)
    # writing a point cloud document in ply format to make it compatible with the lib
    o3d.io.write_point_cloud("dataCloud.ply", point_cloud)
    # read ply file
    pcd = o3d.io.read_point_cloud("dataCloud.ply")
    pcd.estimate_normals()  # compute normals of a point cloud
    print('draw data cloud')
    o3d.visualization.draw_geometries(
        geometry_list=[pcd],
        window_name="Data Cloud"
    )
    return pcd


# fit to unit cube
def unit_cube(pointcloud):
    print('fitting to unit cube')
    pcd.scale(1 / np.max(pointcloud.get_max_bound() - pointcloud.get_min_bound()),
              center=pointcloud.get_center())
    num_points = np.asarray(pointcloud.points).size
    pointcloud.colors = o3d.utility.Vector3dVector(np.random.uniform(0, 1, size=(num_points, 3)))
    print('Getting colours')
    return pointcloud


def create_octree(pointcloud):
    user_depth = int(input("How deep would you like your Octree?"))     # recommended 4-6
    octree = o3d.geometry.Octree(max_depth=user_depth)
    octree.convert_from_point_cloud(pointcloud, size_expand=0.01)
    o3d.visualization.draw_geometries([octree])


if __name__ == '__main__':
    pcd = create_point_cloud(las)
    pcd = unit_cube(pcd)
    create_octree(pcd)
